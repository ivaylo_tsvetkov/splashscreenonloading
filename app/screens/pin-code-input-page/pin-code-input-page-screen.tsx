
import React, { useState } from "react"
import { observer } from "mobx-react-lite"
import { ViewStyle, TextInput, StyleSheet, Button } from "react-native"
import AsyncStorage  from '@react-native-async-storage/async-storage'
import { Screen } from "../../components"
// import { useNavigation } from "@react-navigation/native"
// import { useStores } from "../../models"
import { color } from "../../theme"

const ROOT: ViewStyle = {
  alignItems: "center",
  justifyContent: "center",
  backgroundColor: color.palette.white,
  flex: 1,
}

interface Props {
  navigation: any
}

export const PinCodeInputPageScreen = observer(function PinCodeInputPageScreen(props: Props) {
  const [text, onChangeText] = useState(null);

  const _storeData = async (prop, item) => {
    try {
      await AsyncStorage.setItem(
        prop,
        item
      );
    } catch (error) {
      console.log(error)
      // Error saving data
    }
  };

  const pressButtonHandler = () => {
    _storeData("pin", text);
    props.navigation.navigate('PinView')
  };

  // Pull in one of our MST stores
  // const { someStore, anotherStore } = useStores()

  // Pull in navigation via hook
  // const navigation = useNavigation()
  return (
    <Screen style={ROOT} preset="scroll">
      <TextInput
        onChangeText={onChangeText}
        style={styles.input}
        value={text}
        placeholderTextColor= "black"
        maxLength={6}
        secureTextEntry={true}
        keyboardType="numeric"
        placeholder="Enter your Pin Code here" />
      <Button color="#841584" onPress={pressButtonHandler} disabled={!/\d{6,6}/.test(text)} title="Press me" />
    </Screen>
  )
})

const styles = StyleSheet.create({
  input: {
    borderWidth: 1,
    color: color.palette.black,
    height: 40,
    margin: 12,
    padding: 10,
    width: "48%"
  }
})