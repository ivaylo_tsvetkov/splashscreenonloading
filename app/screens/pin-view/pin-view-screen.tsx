
import React, { useState, useEffect } from "react"
import { observer } from "mobx-react-lite"
import { ViewStyle } from "react-native"
import AsyncStorage  from '@react-native-async-storage/async-storage'
import { Screen, Text } from "../../components"
// import { useNavigation } from "@react-navigation/native"
// import { useStores } from "../../models"
import { color } from "../../theme"

const ROOT: ViewStyle = {
  alignItems: "center",
  justifyContent: "center",
  backgroundColor: color.palette.black,
  flex: 1,
}

export const PinViewScreen = observer(function PinViewScreen() {
  const [pin,setPin] = useState(null)

  useEffect(() => {
    const retrieveData = async () => {
      try {
        const value = await AsyncStorage.getItem('pin');
        if (value !== null) {
          setPin(value)
          // We have data!!
          console.log(value);
        }
      } catch (error) {
        console.log(error)
      }
    };
    retrieveData();
  },[])
  
  // Pull in one of our MST stores
  // const { someStore, anotherStore } = useStores()

  // Pull in navigation via hook
  // const navigation = useNavigation()
  return (
    <Screen style={ROOT} preset="scroll">
      <Text  text="Your pin is:" />
      <Text  text={pin || "Wait"} />
    </Screen>
  )
})
