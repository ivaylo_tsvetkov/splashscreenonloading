import React, { useState, Fragment } from "react"
import { observer } from "mobx-react-lite"
import { ViewStyle, TextStyle, Linking, TouchableOpacity,
   View, BackHandler, Alert } from "react-native"
import { Screen, Text } from "../../components"
// import { useNavigation } from "@react-navigation/native"
// import { useStores } from "../../models"
import { color } from "../../theme"
import styles from './scanStyle'
import QRCodeScanner from 'react-native-qrcode-scanner';

const ROOT: ViewStyle = {
  backgroundColor: color.palette.black,
  flex: 1,
}

interface Props {
    navigation: any
  }

export const ScanScreen = observer(function ScanScreen(props:Props) {
  const [state, setState] = useState({
    scan: false,
    ScanResult: false,
    result: null
  })

  const onSuccess = (e) => {
    const check = e.data.substring(0, 4);
    Alert.alert("QR CODE: ", e.data);
    props.navigation.navigate('Pin')
    console.log('scanned data' + check);
    setState({
      result: e,
      scan: false,
      ScanResult: true
    })
    if (check === 'http') {
      Linking.openURL(e.data).catch(err => console.error('An error occured', err));
    } else {
    setState({
        result: e,
        scan: false,
        ScanResult: true
      })
    }
  }

  const activeQR = () => {
    setState({ ...state, scan: true })
}
const scanAgain = () => {
    setState({...state, scan: true, ScanResult: false })
}

  // Pull in osne of our MST stores
  // const { someStore, anotherStore } = useStores()

  // Pull in navigation via hook
  // const navigation = useNavigation()
  return (
    <Screen style={ROOT} preset="scroll">
      <View style={styles.scrollViewStyle}>
                <Fragment>
                    <View style={styles.header}>
                        <TouchableOpacity onPress={()=> BackHandler.exitApp()}>
                        <Text style={styles.textTitle}>Back</Text>
                        </TouchableOpacity>
                    </View>
                    {!state.scan && !state.ScanResult &&
                        <View style={styles.cardView} >
                            <Text numberOfLines={8} style={styles.descText}>Please move your camera {"\n"} over the QR Code</Text>
                            <TouchableOpacity onPress={activeQR} style={styles.buttonScan}>
                                <View style={styles.buttonWrapper}>
                                <Text style={{...styles.buttonTextStyle, color: '#2196f3'} as TextStyle}>Camera</Text>
                                <Text style={{...styles.buttonTextStyle, color: '#2196f3'} as TextStyle}>Scan QR Code</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    }
                    {state.ScanResult &&
                        <Fragment>
                            <Text style={styles.textTitle1}>Result</Text>
                            <View style={state.ScanResult ? styles.scanCardView : styles.cardView}>
                                <Text>Type : {state.result.type}</Text>
                                <Text>Result : {state.result.data}</Text>
                                <Text numberOfLines={1}>RawData: {state.result.rawData}</Text>
                                <TouchableOpacity onPress={scanAgain} style={styles.buttonScan}>
                                    <View style={styles.buttonWrapper}>
                                    <Text style={{...styles.buttonTextStyle, color: '#2196f3'} as TextStyle}>Camera</Text>
                                        <Text style={{...styles.buttonTextStyle, color: '#2196f3'} as TextStyle}>Click to scan again</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </Fragment>
                    }
                    {state.scan &&
                        <QRCodeScanner
                            vibrate={false}
                            reactivate={true}
                            showMarker={true}
                            ref={(node) => { this.scanner = node }}
                            onRead={onSuccess}
                            topContent={
                                <Text style={styles.centerText}>
                                   Please move your camera {"\n"} over the QR Code
                                </Text>
                            }
                            bottomContent={
                                <View>
                                    <View  style={styles.bottomContent}>
                                        <TouchableOpacity style={styles.buttonScan2} 
                                            onPress={() => this.scanner.reactivate()} 
                                            onLongPress={() => setState({...state, scan: false })}>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            }
                        />
                    }
                </Fragment>
            </View>
    </Screen>
  )
})

