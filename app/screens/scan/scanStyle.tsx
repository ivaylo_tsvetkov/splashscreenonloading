
import { Dimensions, StyleSheet } from 'react-native';
import { color } from '../../theme';
const deviceWidth = Dimensions.get('screen').width;
const deviceHeight = Dimensions.get('screen').height;
const styles = StyleSheet.create({
    bottomContent: {
        height: 120,
       width: deviceWidth,
    },
    buttonScan: {
        borderColor: color.palette.black,
        borderRadius: 10,
        borderWidth: 2,
        marginTop: 20,
        paddingBottom: 5,
        paddingLeft: 25,
        paddingRight: 25,
        paddingTop: 5,
    },
    buttonScan2: {
        height: 100,
        marginLeft: deviceWidth / 2 - 50,
        width: 100,
    },
    buttonTextStyle: {
        color: color.palette.black,
        fontWeight: 'bold',
    },
    buttonTouchable: {
        alignItems: 'center',
        backgroundColor: color.palette.white,
        fontSize: 21,
        height: 44,
        justifyContent: 'center',
        marginTop: 32,
        width: deviceWidth - 62,
    },
    buttonWrapper: {
        alignItems: 'center',
        display: 'flex', 
        flexDirection: 'row',
    },
    cardView: {
        alignItems: 'center',
        alignSelf: 'center',
        backgroundColor: color.palette.white,
        borderRadius: 10,
        height: deviceHeight - 350,
        justifyContent: 'flex-start',
        marginLeft: 5,
        marginRight: 5,
        marginTop: '10%',
        padding: 25,
        width: deviceWidth - 32,
    },
    centerText: {
        color: color.palette.white,
        flex: 1,
        fontSize: 18,
        padding: 32,
        textAlign: 'center',
    },
    descText: {
        fontSize: 16,
        padding: 16,
        textAlign: 'center',
    },
    header: {
        alignItems: 'center',
        display: 'flex',
        flexDirection: 'row',
        height: '10%',
        paddingLeft: 15,
        paddingTop: 10,
        width: deviceWidth,
    },
    highlight: {
        fontWeight: '700',
    },
    scanCardView: {
        alignItems: 'center',
        alignSelf: 'center',
        backgroundColor: color.palette.white,
        borderRadius: 10,
        height: deviceHeight / 2,
        justifyContent: 'center',
        marginLeft: 5,
        marginRight: 5,
        marginTop: 10,
        padding: 25,
        width: deviceWidth - 32,
    },
    scrollViewStyle: {
        backgroundColor: color.palette.black,
        flex: 1,
        justifyContent: 'flex-start',
    },
    textBold: {
        color: color.palette.black,
        fontWeight: '500',
    },
    textTitle: {
        color: color.palette.white,
        fontSize: 18,
        fontWeight: 'bold',
        padding: 16,
        textAlign: 'center',
    },
    
    textTitle1: {
        color: color.palette.white,
        fontSize: 18,
        fontWeight: 'bold',
        padding: 16,
        textAlign: 'center',
    },
})
export default styles;